"use client";
import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";

export default function App() {
  const [data, setData] = useState(null);

  const [fetchStatus, setFetchStatus] = useState(true);

  useEffect(() => {
    if (fetchStatus === true) {
      axios
        .get("https://backendexample.sanbercloud.com/api/student-scores")
        .then((res) => {
          setData([...res.data]);
        })
        .catch((error) => {});
      setFetchStatus(false);
    }
  }, [fetchStatus, setFetchStatus]);

  const handleIndexScore = (param) => {
    if (param < 50) {
      return "E";
    } else if (param >= 50 && param < 60) {
      return "D";
    } else if (param >= 60 && param < 70) {
      return "C";
    } else if (param >= 70 && param < 80) {
      return "B";
    } else {
      return "A";
    }
  };

  const handleInput = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    if (name === "name") {
      setInput({ ...input, name: value });
    } else if (name === "course") {
      setInput({ ...input, course: value });
    } else if (name === "score") {
      setInput({ ...input, score: value });
    }
  };

  const [input, setInput] = useState({
    name: "",
    course: "",
    score: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    let { name, course, score } = input;

    if (currentId === -1) {
      axios.post("https://backendexample.sanbercloud.com/api/student-scores", { name, course, score }).then((res) => {
        console.log(res);
        setFetchStatus(true);
      });
    } else {
      axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name, course, score }).then((res) => {
        setFetchStatus(true);
      });
    }
    setCurrentId(-1);

    setInput({
      name: "",
      course: "",
      score: "",
    });
  };

  const handleDelete = (event) => {
    let idData = parseInt(event.target.value);

    axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`).then((res) => {
      setFetchStatus(true);
    });
  };

  const [currentId, setCurrentId] = useState(-1);

  const handleEdit = (event) => {
    let idData = parseInt(event.target.value);
    setCurrentId(idData);

    axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`).then((res) => {
      let data = res.data;
      setInput({
        name: data.name,
        course: data.course,
        score: data.score,
      });
    });
  };

  return (
    <div className="flex m-4 justify-between">
      <div className="overflow-x-auto shadow-md sm:rounded-lg mx-auto mt-6">
        <table className="w-full text-sm text-left text-gr-500 dark:text-gray-400">
          <thead className="bg-purple-600 text-white">
            <tr>
              <th scope="col" className="px-2 py-3">
                NO
              </th>
              <th scope="col" className="px-6 py-3">
                NAMA
              </th>
              <th scope="col" className="px-6 py-3">
                MATA KULIAH
              </th>
              <th scope="col" className="px-6 py-3">
                NILAI
              </th>
              <th scope="col" className="px-6 py-3">
                INDEX NILAI
              </th>
              <th scope="col" className="px-6 py-3">
                ACTION
              </th>
            </tr>
          </thead>
          <tbody>
            {data !== null &&
              data.map(function (res, index) {
                return (
                  <tr className="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                    <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                      {index + 1}
                    </th>
                    <td className="px-6 py-4">{res.name}</td>
                    <td className="px-6 py-4">{res.course}</td>
                    <td className="px-6 py-4">{res.score}</td>
                    <td className="px-6 py-4">{handleIndexScore(res.score)}</td>
                    <td className="px-6 py-4">
                      <div className="button-group">
                        <button
                          onClick={handleEdit}
                          value={res.id}
                          type="button"
                          className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-xs px-2 py-1 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                        >
                          Edit
                        </button>

                        <button
                          onClick={handleDelete}
                          value={res.id}
                          type="button"
                          className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-sm rounded-lg text-xs px-2 py-1 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                        >
                          Delete
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
      <div className="w-1/3 mx-auto my-6">
        <div className="overflow-x-auto shadow-md sm:rounded-lg pb-6">
          <div className="overflow-hidden bg-purple-600 text-white p-2 font-bold">
            FORM
          </div>
          <form onSubmit={handleSubmit} className="px-6 mx-auto pt-6">
            <div className="mb-6">
              <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Name :
              </label>
              <input
                onChange={handleInput}
                value={input.name}
                name="name"
                type="text"
                id="Name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Isi Nama"
                required=""
              />
            </div>
            <div className="mb-6">
              <label htmlFor="text" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Mata Kuliah :
              </label>
              <input
                onChange={handleInput}
                value={input.course}
                name="course"
                type="text"
                id="Mata Kuliah"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required=""
                placeholder="Isi Mata Kuliah"
              />
            </div>
            <div className="mb-6">
              <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Nilai :
              </label>
              <input
                onChange={handleInput}
                value={input.score}
                name="score"
                type="text"
                id="Mata Kuliah"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required=""
                placeholder="Isi Nilai"
              />
            </div>
            <button
              type="submit"
              className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}